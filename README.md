# Redmine plugin Delayed Job

A Redmine plugin that enables Delayed Job as Active Job backend.

## Install

Install this plugin:

```bash
cd redmine
git clone https://gitlab.com/redmine-plugin-delayed-job/redmine-plugin-delayed-job.git plugins/delayed_job
bundle install
RAILS_ENV=production bin/rails redmine:plugins:migrate NAME=delayed_job
```

Install systemd service for Delayed Job worker:

```bash
cd redmine
RAILS_ENV=production plugins/delayed_job/bin/generate_systemd_service | \
  sudo -H tee /lib/systemd/system/redmine-delayed-job@.service
RAILS_ENV=production plugins/delayed_job/bin/generate_systemd_timer | \
  sudo -H tee /lib/systemd/system/redmine-delayed-job@.timer
sudo -H systemctl daemon-reload
sudo -H systemctl enable --now redmine-delayed-job@0.service
sudo -H systemctl enable --now redmine-delayed-job@0.timer
```

If you work to use more workers, use `redmine-delayed-job@1`,
`redmine-delayed-job@2` and so on:

```bash
sudo -H systemctl enable --now redmine-delayed-job@1.service
sudo -H systemctl enable --now redmine-delayed-job@1.timer
sudo -H systemctl enable --now redmine-delayed-job@2.service
sudo -H systemctl enable --now redmine-delayed-job@2.timer
```

Restart Redmine.

## Uninstall

Uninstall systemd service for Delayed Job worker:

```bash
sudo -H systemctl disable --now redmine-delayed-job@0.timer
sudo -H rm /lib/systemd/system/redmine-delayed-job@.timer
sudo -H systemctl disable --now redmine-delayed-job@0.service
sudo -H rm /lib/systemd/system/redmine-delayed-job@.service
sudo -H systemctl daemon-reload
```

Uninstall this plugin:

```bash
cd redmine
RAILS_ENV=production bin/rails redmine:plugins:migrate NAME=delayed_job VERSION=0
rm -rf plugins/delayed_job
bundle install
```

Restart Redmine.

## How to show worker status

```bash
sudo -H systemctl status redmine-delayed-job@0
```

## How to start worker

```bash
sudo -H systemctl start redmine-delayed-job@0
```

## How to restart worker

```bash
sudo -H systemctl restart redmine-delayed-job@0
```

## How to stop worker

```bash
sudo -H systemctl stop redmine-delayed-job@0
```

## Authors

  * Sutou Kouhei `<kou@clear-code.com>`

  * Shimadzu Corporation

## License

GPLv2 or later. See [LICENSE](LICENSE) for details.

